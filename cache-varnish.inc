<?php
require_once DRUPAL_ROOT . '/' . dirname(drupal_get_filename('module', 'varnish')) . '/varnish.module';
require_once DRUPAL_ROOT . '/' . dirname(drupal_get_filename('module', 'varnish')) . '/varnish.cache.inc';

class DrupalVarnishCacheTagsPlugin extends VarnishCache {
  function set($cid, $data, $expire = CACHE_PERMANENT, $tags = array(), $request_tags = array()) {
    if (!empty($tags)) {
      drupal_add_http_header('X-Cache-Tags', implode(',', DrupalCacheTags::flatten($tags)));
    }
  }

  function invalidate($tags = array()) {
    $commands = array();
    $host = _varnish_get_host();

    $purge_command = variable_get('cachetags_varnish_varnish_3', FALSE) ? 'ban' : 'purge';
    $host_field = variable_get('cachetags_varnish_use_x_host', FALSE) ? 'obj.http.X-Host' : 'req.http.host';

    foreach (DrupalCacheTags::flatten($tags) as $tag) {
      $commands[] = "${purge_command} ${host_field} ~ ${host} && obj.http.X-Cache-Tags ~ ${tag}";
    }
    _varnish_terminal_run($commands);
  }
}
